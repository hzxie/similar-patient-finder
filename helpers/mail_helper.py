# -*- coding: utf-8 -*-
import logging

from email.message import EmailMessage
from smtplib import SMTP_SSL


def get_mail_sender(mail_host, mail_port, mail_user, mail_pass, mail_tmpl_dir):
    smtp = SMTP_SSL()
    try:
        smtp.connect(mail_host, mail_port)
        smtp.login(mail_user, mail_pass)  
    except Exception as ex:
        logging.exception(ex)

    return {
        "smtp": smtp,
        "tmpl_dir": mail_tmpl_dir
    }

def send_mail_from_template(subject, mail_tpl_name, params, from_email, to_emails, connection):
    with open(mail_tpl_name) as mf:
        mail_html = mf.read()

    for k, v in params.items():
        mail_html = mail_html.replace("{{ %s }}" % k, v)

    
    for to_email_addr in to_emails:
        msg = EmailMessage()
        msg["Subject"] = subject
        msg["From"] = from_email
        msg["To"] = to_email_addr
        msg.set_content(mail_html, subtype='html')
        connection.send_message(msg)
